<?php

use Illuminate\Http\Request;

Route::group(['prefix' => '/login'], function() {
    Route::post('/', 'LoginController@login');
});


Route::group(['prefix' => '/pelicula', 'middleware' => ['auth']], function() {
    Route::get('/', 'PeliculaController@get');
    Route::post('/', 'PeliculaController@Insert');
    Route::post('/editar', 'PeliculaController@Update');
});
Route::group(['prefix' => '/turno', 'middleware' => ['auth']], function() {
    Route::get('/', 'TurnoController@get');
    Route::post('/', 'TurnoController@Insert');
    Route::post('/editar', 'TurnoController@Update');
});

Route::group(['prefix' => '/pelicula-turno', 'middleware' => ['auth']], function() {
    Route::get('/{idpelicula?}', 'PeliculaTurnoController@get');
    Route::post('/', 'PeliculaTurnoController@Insert');
});