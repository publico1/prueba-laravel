<?php

/* ACCESO DEL ADMINISTRADOR */
Route::group(['prefix' => '/login'], function() {
    Route::post('/', 'LoginController@login');
});
