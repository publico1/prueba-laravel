<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipodoc extends Model
{
    protected $connection = 'solpack';
    protected $table = 'TIPODOC';

    public $timestamps = false;
}
