<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Turno extends Model
{
    protected $connection = 'prueba';
    protected $table = 'TURNO';

    public $timestamps = false;

    public function Estado(){
        return $this->hasOne('App\Estado', 'id', 'idestado');
    }
}
