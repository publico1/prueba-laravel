<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class Autentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('Authorization');
        if($token == ""){
            $token = session()->get('token');
        }
        $jwtAuth = new \JwtAuth();
        $acceso = $jwtAuth->checkToken($token);
        if(!$acceso){
            // return new response(view('http_error')
            //     ->with('error','401')
            //     ->with('error_titulo','Acceso Denegado')
            //     ->with('error_descripcion', 'Usted no tiene acceso a este elemento. Le recomendamos que inicie sesión en el sistema')
            // );
            //abort(403, "Acceso Denegado"); 
            $jsonReturn = array(
                'code' => 401,
                'status' => 'error',
                'msg' => "Acceso Denegado"
            );
            return response()->json($jsonReturn, $jsonReturn['code']);
        }

        return $next($request);
    }
}
