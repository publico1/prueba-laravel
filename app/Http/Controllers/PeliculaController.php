<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\Funciones;
use Storage;
use File;
use App\Pelicula;

class PeliculaController extends Controller
{
    public function get(Request $request){
        $objlist = Pelicula::paginate(10);

        foreach($objlist as $item){
            $item->Estado;
            $item->archivo = url('/') . '/storage_web/' . $item->archivo;
        }

        $jsonReturn = array(
            'code' => 200,
            'status' => 'success',
            'data' => $objlist
        );
        return response()->json($jsonReturn, 200);
    }
    public function Insert(Request $request){
        $nombre = trim($request->input('nombre'));
        $publicacion = trim($request->input('publicacion'));
        $idestado = trim($request->input('idestado'));
        $archivo = $request->file('archivo');

        $ruta = Funciones::GetDbDataTime()->fechaid . '.' . $archivo->getClientOriginalExtension();

        $auth = new \JwtAuth();
        $usuregistro = $auth->checkToken($request->header('Authorization'), $getIdentity = true)->sub;

        DB::beginTransaction();

        $new_class = new Pelicula();
        $new_class->id = Funciones::GetAutoInc('PELICULA', 'id');
        $new_class->nombre = $nombre;
        $new_class->publicacion = $publicacion;
        $new_class->idestado = $idestado;
        $new_class->archivo = $ruta;
        $new_class->fecregistro = Funciones::GetDbDataTime()->fechadb;
        $new_class->usuregistro = $usuregistro;
        if($new_class->save()){
            if(Storage::disk('web')->put($ruta, File::get($archivo))){
                DB::commit();
                $jsonReturn = array('code' => 200,'status' => 'success','transaction' => true,'mensaje' => 'Se registro correctamente');
                return response()->json($jsonReturn, $jsonReturn['code']);
            }else{
                DB::rollback();
                $jsonReturn = array('code' => 400,'status' => 'success','transaction' => false,'mensaje' => 'Ocurrió un error');
                return response()->json($jsonReturn, $jsonReturn['code']);  
            }            
        }else{
            DB::rollback();
            $jsonReturn = array('code' => 400,'status' => 'success','transaction' => false,'mensaje' => 'Ocurrió un error');
            return response()->json($jsonReturn, $jsonReturn['code']);  
        }
    }
    public function Update(Request $request){
        $id = trim($request->input('id'));
        $nombre = trim($request->input('nombre'));
        $publicacion = trim($request->input('publicacion'));
        $idestado = trim($request->input('idestado'));
        $archivo = $request->file('archivo');

        $ruta = Funciones::GetDbDataTime()->fechaid . '.' . $archivo->getClientOriginalExtension();

        DB::beginTransaction();

        $new_class = Pelicula::find($id);

        $ruta_anterior = $new_class->archivo;

        $new_class->nombre = $nombre;
        $new_class->publicacion = $publicacion;
        $new_class->idestado = $idestado;
        $new_class->archivo = $ruta;
        if($new_class->save()){
            if(Storage::disk('web')->put($ruta, File::get($archivo))){
                $archivo_path = public_path('storage_web') . '/' . $ruta_anterior;
                File::move($archivo_path, public_path('storage_web') . '/del_' . $ruta_anterior);
                DB::commit();
                $jsonReturn = array('code' => 200,'status' => 'success','transaction' => true,'mensaje' => 'Se registro correctamente');
                return response()->json($jsonReturn, $jsonReturn['code']);
            }else{
                DB::rollback();
                $jsonReturn = array('code' => 400,'status' => 'success','transaction' => false,'mensaje' => 'Ocurrió un error');
                return response()->json($jsonReturn, $jsonReturn['code']);  
            }            
        }else{
            DB::rollback();
            $jsonReturn = array('code' => 400,'status' => 'success','transaction' => false,'mensaje' => 'Ocurrió un error');
            return response()->json($jsonReturn, $jsonReturn['code']);  
        }
    }
}
