<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Turno;
use Illuminate\Support\Facades\DB;
use App\Helpers\Funciones;

class TurnoController extends Controller
{
    public function get(Request $request){
        $objlist = Turno::paginate(10);

        foreach($objlist as $item){
            $item->Estado;
        }

        $jsonReturn = array(
            'code' => 200,
            'status' => 'success',
            'data' => $objlist
        );
        return response()->json($jsonReturn, 200);
    }
    public function Insert(Request $request){
        $hora = trim($request->input('hora'));
        $idestado = trim($request->input('idestado'));

        $auth = new \JwtAuth();
        $usuregistro = $auth->checkToken($request->header('Authorization'), $getIdentity = true)->sub;

        DB::beginTransaction();

        $new_class = new Turno();
        $new_class->id = Funciones::GetAutoInc('TURNO', 'id');
        $new_class->hora = $hora;
        $new_class->idestado = $idestado;
        $new_class->fecregistro = Funciones::GetDbDataTime()->fechadb;
        $new_class->usuregistro = $usuregistro;
        if($new_class->save()){
            DB::commit();
            $jsonReturn = array('code' => 200,'status' => 'success','transaction' => true,'mensaje' => 'Se registro correctamente');
            return response()->json($jsonReturn, $jsonReturn['code']);            
        }else{
            DB::rollback();
            $jsonReturn = array('code' => 400,'status' => 'success','transaction' => false,'mensaje' => 'Ocurrió un error');
            return response()->json($jsonReturn, $jsonReturn['code']);  
        }
    }
    public function Update(Request $request){
        $id = trim($request->input('id'));
        $hora = trim($request->input('hora'));
        $idestado = trim($request->input('idestado'));

        DB::beginTransaction();

        $new_class = Turno::find($id);
        $new_class->hora = $hora;
        $new_class->idestado = $idestado;
        if($new_class->save()){
            DB::commit();
            $jsonReturn = array('code' => 200,'status' => 'success','transaction' => true,'mensaje' => 'Se registro correctamente');
            return response()->json($jsonReturn, $jsonReturn['code']);            
        }else{
            DB::rollback();
            $jsonReturn = array('code' => 400,'status' => 'success','transaction' => false,'mensaje' => 'Ocurrió un error');
            return response()->json($jsonReturn, $jsonReturn['code']);  
        }
    }
}
