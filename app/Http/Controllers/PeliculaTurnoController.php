<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Turno;
use App\Pelicula;
use App\PeliculaTurno;
use Illuminate\Support\Facades\DB;
use App\Helpers\Funciones;

class PeliculaTurnoController extends Controller
{
    public function get(Request $request, $idpelicula = null){
        $objlist = null;
        if(!is_null($idpelicula)){
            $objlist = PeliculaTurno::where('idpelicula', $idpelicula)->paginate(10);
        }else{
            $objlist = PeliculaTurno::paginate(10);
        }

        $jsonReturn = array(
            'code' => 200,
            'status' => 'success',
            'data' => $objlist
        );
        return response()->json($jsonReturn, 200);
    }
    public function Insert(Request $request){
        $idpelicula = trim($request->input('idpelicula'));
        $turnos = json_decode($request->input('turnos'), true);

        $auth = new \JwtAuth();
        $usuregistro = $auth->checkToken($request->header('Authorization'), $getIdentity = true)->sub;

        $fechadb = Funciones::GetDbDataTime()->fechadb;

        DB::beginTransaction();

        // Borramos todoas los turnos
        PeliculaTurno::where('idpelicula', $idpelicula)->delete();

        // volvemos a llenar los turnos
        foreach($turnos as $turno){
            $new_class = new PeliculaTurno();
            $new_class->idpelicula = $idpelicula;
            $new_class->idturno = $turno['id'];
            $new_class->fecregistro = $fechadb;
            $new_class->usuregistro = $usuregistro;
            if(!$new_class->save()){
                DB::rollback();
                $jsonReturn = array('code' => 400,'status' => 'success','transaction' => false,'mensaje' => 'Ocurrió un error');
                return response()->json($jsonReturn, $jsonReturn['code']);
            }
        }

        DB::commit();
        $jsonReturn = array('code' => 200,'status' => 'success','transaction' => true,'mensaje' => 'Se registro correctamente');
        return response()->json($jsonReturn, $jsonReturn['code']); 
    }
}
