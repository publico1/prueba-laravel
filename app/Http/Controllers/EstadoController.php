<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Estado;

class EstadoController extends Controller
{
    public function get(Request $request){
        $objlist = Estado::where('id', '<>','0')->get();

        $list_return = array();
        foreach($objlist as $item){
            $obj = array(
                'id' => $item->id,
                'nombre' => $item->nombre,
                'siglas' => $item->siglas,
            );
            array_push($list_return, $obj);
        }
        
        $jsonReturn = array(
            'code' => 200,
            'status' => 'success',
            'data' => $list_return
        );
        return response()->json($jsonReturn, 200);
    }
}
