<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use App\Helpers\Funciones;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    public function login(Request $request){
        // dd("hola");
        $jwtAuth = new \JwtAuth();
        $usuario = trim($request->input('usuario'));
        $password = trim($request->input('password'));
        $verif = $jwtAuth->singup($usuario, $password);
        // dd($verif);
        if(isset($verif['token'])){
            $usuario_logueado = Usuario::where('usuario', $usuario)->first();
            // dd($usuario_logueado);
            // $request->session()->put('token', $verif['token']);
            $obj = array(
                'usuario' => $usuario_logueado->usuario,
                'nombres' => $usuario_logueado->nombres,
                'apellido1' => $usuario_logueado->apellido1,
                'apellido2' => $usuario_logueado->apellido2
            );
            // $request->session()->put('usuario', $obj);

            $jsonReturn = array('code' => 200,'status' => 'success','acceso' => true, 'token' => $verif['token']);
            return response()->json($jsonReturn, 200);
        }else{
            $jsonReturn = array('code' => 401,'status' => 'Unauthorized','acceso' => false, 'message' => 'Acceso denegado, Usuario o Password incorrectos');
            return response()->json($jsonReturn, 401);
        }
    }
}
