<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelicula extends Model
{
    protected $connection = 'prueba';
    protected $table = 'PELICULA';

    public $timestamps = false;

    public function Estado(){
        return $this->hasOne('App\Estado', 'id', 'idestado');
    }
}
