<?php
namespace App\Helpers;

use Firebase\JWT\JWT;
use Illuminate\Support\Facades\DB;
use App\Usuario;

Class JwtAuth{

    public $key;

    public function __construct(){
        $this->key = 'token-steve';
    }
    public function singup($usuario, $password, $getToken = null){
        $acceso = false;
        $usuario = Usuario::where('usuario', $usuario)->where('idestado', 1)->first();
        $password = hash('sha256', $password);
        
        if(is_object($usuario)){
            if($usuario->password == $password){
                $acceso = true;
            }
        }
        if($acceso){
            $token = array(
                'sub'       => $usuario->id,
                'nombre'    => $usuario->nombres . ' ' . $usuario->apellido1 . ' ' . $usuario->apellido2,
                'iat'       => time(),
                'exp'       => time() + (30 * 24 * 60 * 60)
            );
            $jwt = JWT::encode($token, $this->key, 'HS256');
            $decoded = JWT::decode($jwt, $this->key, ['HS256']);
            if(is_null($getToken)){
                $data = array(
                    'code' => 200,
                    'status' => 'success',
                    'acceso' => true,
                    'token' => $jwt
                );
                
            }else{
                $data = $decoded;
            }
        }else{
            $data = array(
                'status' => 'Denegado',
                'message' => 'Login incorrecto',
                'acceso' => false,
                'code' => 400
            );
        }
        return $data;
    }
    public function checkToken($jwt, $getIdentity = false){
        $auth = false;
        $decoded = null;
        try{
            $decoded = JWT::decode($jwt, $this->key, ['HS256']);
        }catch(\UnexpectedValueException $e){
            $auth = false;
        }catch(\DomainException $e){
            $auth = false;
        }

        if(!empty($decoded) && is_object($decoded) && isset($decoded)){
            $auth = true;
        }else{
            $auth = false;
        }

        if($getIdentity){
            return $decoded;
        }

        return $auth;
    }
    public function getTokenDevelop(){
        $token = array(
            'sub'       => '1',            
            'nombre'    => 'develop',
            'iat'       => time(),
            'exp'       => time() + (6 * 24 * 60 * 60)
        );
        $jwt = JWT::encode($token, $this->key, 'HS256');

        $data = array(
            'token' => $jwt
        );
        return $data;
    }
}