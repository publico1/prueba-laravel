<?php
namespace App\Helpers;
use Illuminate\Support\Facades\DB;

class Funciones{
    public static function GetDbDataTime(){
        $fecha = DB::select(
            "select date_format(NOW(), '%Y-%m-%d') as date_gion1," .
            "date_format(NOW(), '%d-%m-%Y') as date_gion2," .
            "date_format(NOW(), '%d/%m/%Y') as date_slash1," .
            "date_format(NOW(), '%Y/%m/%d') as date_slash2," . 
            "date_format(NOW(), '%d/%m/%Y %h:%i:%s') as date_time," .
            "date_format(NOW(), '%Y') as anio," .
            "date_format(LAST_DAY(NOW()), '%Y/%m/%d') as LastDay ," .
            "date_format(NOW(), '%d%m%Y_%H%i%s') as fechaid," .
            "date_format(NOW(), '%d') as day," .
            "date_format(NOW(), '%m') as month," .
            "date_format(NOW(), '%Y') as year," .
            "NOW() as fechadb"
        );
        return $fecha[0];
    }

    // fecha en formato (YYYY-mm-dd)
    public static function GetLastDay($date){
        $fecha = DB::select("SELECT date_format(LAST_DAY('$date'), '%Y/%m/%d') as LastDay");
        return $fecha[0]->LastDay;
    }

    public static function GetAutoInc($tabla, $columna){
        $maximo = DB::select("SELECT MAX($columna) AS max FROM $tabla");
        return $maximo[0]->max + 1;
    }

    public static function FormatDate($date, $numformat = null){
        $meses_full = ['01' => 'Enero', '02' => 'Febrero', '03' => 'Marzo', '04' => 'Abril', '05' => 'Mayo', '06' => 'Junio', '07' => 'Julio', '08' => 'Agosto', '09' => 'Setiembre', '10' => 'Octubre', '11' => 'Noviembre', '12' => 'Diciembre'];
        $meses_min = ['01' => 'ene', '02' => 'feb', '03' => 'mar', '04' => 'abr', '05' => 'may', '06' => 'jun', '07' => 'jul', '08' => 'ago', '09' => 'set', '10' => 'oct', '11' => 'nov', '12' => 'dic'];
        $dia = date_format($date, 'd');
        $mes = date_format($date, 'm');
        $anio = date_format($date, 'Y');
        $mes = intval($mes);
        if($mes < 10){ $mes = '0' . $mes; }
        if($numformat == 1){
            return $meses_full[$mes] . ' ' . $dia . ', ' . $anio;
        }else{
            return $dia . '/' . $mes . '/' . $anio;
        }
    }

    
}