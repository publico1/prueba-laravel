<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $connection = 'prueba';
    protected $table = 'USUARIO';

    public $timestamps = false;

    public function Estado(){
        return $this->hasOne('App\Estado', 'id', 'idestado');
    }

    public function Registrador(){
        return $this->hasOne('App\Usuario', 'id', 'usuregistro');
    }
}
