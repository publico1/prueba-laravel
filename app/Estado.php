<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $connection = 'prueba';
    protected $table = 'ESTADO';

    public $timestamps = false;
}
