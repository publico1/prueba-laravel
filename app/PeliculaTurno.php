<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class PeliculaTurno extends Model
{
    protected $connection = 'prueba';
    protected $table = 'PELICULA_TURNO';

    public $timestamps = false;
    public $incrementing = false;

    protected function setKeysForSaveQuery(Builder $query){
        $query
            ->where('idpelicula', '=', $this->getAttribute('idpelicula'))
            ->where('idturno', '=', $this->getAttribute('idturno'));
        return $query;
    }
}
