
CREATE TABLE ESTADO (
  id INT(11) NOT NULL,
  nombre varchar(10) NOT NULL,
  siglas varchar(10) NOT NULL
) ENGINE=InnoDB;
INSERT INTO ESTADO VALUES(0, 'Eliminado', 'ELI');
INSERT INTO ESTADO VALUES(1, 'Activo', 'ACT');
INSERT INTO ESTADO VALUES(2, 'Inactivo', 'INA');

-- --------------------------------------------------------

CREATE TABLE PELICULA (
  id INT(11) NOT NULL,
  nombre varchar(100) NOT NULL,
  publicacion timestamp NOT NULL ,
  idestado INT(11) NOT NULL,
  archivo varchar(20) NOT NULL,
  fecregistro TIMESTAMP NULL,
  usuregistro INT(11) NOT NULL
) ENGINE=InnoDB;
INSERT INTO PELICULA VALUES(1, 'X Men: Dias del futuro pasado', '2016-05-10 00:00:00', 2, '11022020_140819.gif', '2020-02-11 13:24:06', 1);

-- --------------------------------------------------------
CREATE TABLE PELICULA_TURNO (
  idpelicula INT(11) NOT NULL,
  idturno INT(11) NOT NULL,
  fecregistro TIMESTAMP NULL,
  usuregistro INT(11) NOT NULL
) ENGINE=InnoDB;
INSERT INTO PELICULA_TURNO VALUES(1, 1, '2020-02-11 14:24:23', 1);
INSERT INTO PELICULA_TURNO VALUES(1, 2, '2020-02-11 14:24:23', 1);
INSERT INTO PELICULA_TURNO VALUES(1, 3, '2020-02-11 14:24:23', 1);

-- --------------------------------------------------------
CREATE TABLE TURNO (
  id INT(11) NOT NULL,
  hora time NOT NULL,
  idestado INT(11) NOT NULL,
  fecregistro TIMESTAMP NULL,
  usuregistro INT(11) NOT NULL
) ENGINE=InnoDB;
INSERT INTO TURNO VALUES(1, '13:30:00', 1, '2020-02-11 14:23:46', 1);
INSERT INTO TURNO VALUES(2, '15:00:00', 1, '2020-02-11 14:23:57', 1);
INSERT INTO TURNO VALUES(3, '15:30:00', 1, '2020-02-11 14:24:02', 1);

-- --------------------------------------------------------
CREATE TABLE USUARIO (
  id INT(11) NOT NULL,
  usuario varchar(20) NOT NULL,
  correo varchar(100) NOT NULL,
  password varchar(200) NOT NULL,
  numdoc varchar(20) NOT NULL,
  apellido1 varchar(20) NOT NULL,
  apellido2 varchar(20) NOT NULL,
  nombres varchar(20) NOT NULL,
  idestado INT(11) NOT NULL,
  fecregistro TIMESTAMP NULL,
  usuregistro INT(11) DEFAULT NULL
) ENGINE=InnoDB;
INSERT INTO USUARIO VALUES(1, 'admin', 'sshicala-agencia.pe', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '12345678', 'Shica', 'Sivipaucar', 'Steve', 1, '2020-02-10 20:21:49', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla ESTADO
--
ALTER TABLE ESTADO
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla PELICULA
--
ALTER TABLE PELICULA
  ADD PRIMARY KEY (id),
  ADD KEY idestado (idestado),
  ADD KEY usuregistro (usuregistro);

--
-- Indices de la tabla PELICULA_TURNO
--
ALTER TABLE PELICULA_TURNO
  ADD PRIMARY KEY (idpelicula,idturno),
  ADD KEY idturno (idturno);

--
-- Indices de la tabla TURNO
--
ALTER TABLE TURNO
  ADD PRIMARY KEY (id),
  ADD KEY usuregistro (usuregistro);

--
-- Indices de la tabla USUARIO
--
ALTER TABLE USUARIO
  ADD PRIMARY KEY (id),
  ADD KEY idestado (idestado);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla PELICULA
--
ALTER TABLE PELICULA
  ADD CONSTRAINT PELICULA_ibfk_1 FOREIGN KEY (idestado) REFERENCES ESTADO (id),
  ADD CONSTRAINT PELICULA_ibfk_2 FOREIGN KEY (usuregistro) REFERENCES USUARIO (id);

--
-- Filtros para la tabla PELICULA_TURNO
--
ALTER TABLE PELICULA_TURNO
  ADD CONSTRAINT PELICULA_TURNO_ibfk_1 FOREIGN KEY (idpelicula) REFERENCES PELICULA (id),
  ADD CONSTRAINT PELICULA_TURNO_ibfk_2 FOREIGN KEY (idturno) REFERENCES TURNO (id);

--
-- Filtros para la tabla TURNO
--
ALTER TABLE TURNO
  ADD CONSTRAINT TURNO_ibfk_1 FOREIGN KEY (usuregistro) REFERENCES USUARIO (id);

--
-- Filtros para la tabla USUARIO
--
ALTER TABLE USUARIO
  ADD CONSTRAINT USUARIO_ibfk_1 FOREIGN KEY (idestado) REFERENCES ESTADO (id);
COMMIT;
