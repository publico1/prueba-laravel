@extends('layouts.templateadmin')
@section('content')

<nav class="breadcrumb bg-white small">
    <a class="breadcrumb-item" href="{{ url('/') }}/admin/dashboard"><i class="fas fa-home mr-1"></i>Inicio</a>
    <a class="breadcrumb-item" href="#"><i class="fas fa-cog mr-1"></i>Administración</a>
    <a class="breadcrumb-item active"><i class="fas fa-users mr-1"></i>Usuarios</a>
</nav>

<div class="card">
    <div class="card-header"><h4 class="mb-0">Usuarios</h4></div>
    <div class="card-body row">
        <div class="col-md-12 text-right"><a href="#" class="btn btn-sm btn-primary" onclick="abre_mdlAgregar()"><i class="fas fa-plus mr-2"></i>Agregar</a></div>
        <div class="col-md-12 table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Usuario</th>
                        <th>Correo</th>
                        <th>Nombre</th>
                        <th style="min-width: 185px;">Registro</th>
                        <th>Registrador</th>
                        <th>Estado</th>
                        <th style="min-width: 90px;">Opción</th>
                    </tr>
                </thead>
                <tbody id="tblData">
                    <?php /* Editado con JQuery */ ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div id="summernote"></div>



<!-- Modal mdlAgregar -->
<div class="modal fade" id="mdlAgregar">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Usuario</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row mt-2 mb-2">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nombres">Nombres:</label>
                            <input type="text" class="form-control" id="nombres">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="apellido1">1er Apellido:</label>
                            <input type="text" class="form-control" id="apellido1">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="apellido2">2do Apellido:</label>
                            <input type="text" class="form-control" id="apellido2">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="idtipodoc">Tipo de documento:</label>
                            <select class="form-control" id="idtipodoc">
                                <?php /* Editado con JQuery */ ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="numdoc">Número de documento:</label>
                            <input type="number" class="form-control" id="numdoc">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="correo">Correo:</label>
                            <input type="email" class="form-control" id="correo">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="usuario">Usuario:</label>
                            <input type="text" class="form-control" id="usuario">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" onclick="Registrar()"><i class="fas fa-save mr-2"></i>Registrar</button>
            </div>
        </div>
    </div>
</div>
<!-- Fin Modal -->


<!-- Modal mdlEditar -->
<div class="modal fade" id="mdlEditar">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Usuario</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row mt-2 mb-2">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nombres">Nombres:</label>
                            <input type="text" class="form-control" id="mdlEditar_nombres">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="apellido1">1er Apellido:</label>
                            <input type="text" class="form-control" id="mdlEditar_apellido1">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="apellido2">2do Apellido:</label>
                            <input type="text" class="form-control" id="mdlEditar_apellido2">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="idtipodoc">Tipo de documento:</label>
                            <select class="form-control" id="mdlEditar_idtipodoc">
                                <?php /* Editado con JQuery */ ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="numdoc">Número de documento:</label>
                            <input type="number" class="form-control" id="mdlEditar_numdoc">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="correo">Correo:</label>
                            <input type="email" class="form-control" id="mdlEditar_correo">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="usuario">Usuario:</label>
                            <input type="text" class="form-control" id="mdlEditar_usuario" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="idtipodoc">Estado:</label>
                            <select class="form-control" id="mdlEditar_idestado">
                                <?php /* Editado con JQuery */ ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" onclick="Guardar()" id="mdlEditar_guardar" idusuario=""><i class="fas fa-save mr-2"></i>Guardar</button>
            </div>
        </div>
    </div>
</div>
<!-- Fin Modal -->


<!-- Modal mdlEliminar -->
<div class="modal fade" id="mdlEliminar">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Eliminar Usuario</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row mt-2 mb-2">
                    <div class="col-md-12">
                        <span>Esta acción es irreversible<br><strong>¿Esta seguro que desea eliminar este registro?</strong></span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-danger" onclick="Eliminar()" id="mdlEliminar_eliminar" idusuario=""><i class="fas fa-trash mr-2"></i>Eliminar</button>
            </div>
        </div>
    </div>
</div>
<!-- Fin Modal -->







<script>
    $( document ).ready(function() {
        CargaData();

        $('#summernote').summernote({
            height: 300,
            popover: { image: [], link: [], air: [] }
        });
        
    });
    function CargaTipodoc(idtipodoc){
        $("#idtipodoc").html('<option>Cargando ...</option>');
        $("#mdlEditar_idtipodoc").html('<option>Cargando ...</option>');
        $.ajax({
            url: "{{ url('/')}}/api/tipodoc",
            type: "GET",
            headers: { authorization: "{{ session()->get('token') }}" },
            success: function (resultado) {
                var contenido = "";
                $.each(resultado.data, function(i, item){
                    contenido += '<option value="' + item.id + '">' + item.siglas + '</option>';
                });
                $("#idtipodoc").html(contenido);

                $("#mdlEditar_idtipodoc").html(contenido);
                $("#mdlEditar_idtipodoc").val(idtipodoc);
            },
            error: function (resultado) {
                $("#mensaje").html(resultado.responseJSON.message);
            }
        });
    }
    function CargaEstado(idestado){
        $("#mdlEditar_idestado").html('<option>Cargando ...</option>');
        $.ajax({
            url: "{{ url('/')}}/api/estado",
            type: "GET",
            headers: { authorization: "{{ session()->get('token') }}" },
            success: function (resultado) {
                var contenido = "";
                $.each(resultado.data, function(i, item){
                    contenido += '<option value="' + item.id + '">' + item.nombre + '</option>';
                });
                $("#mdlEditar_idestado").html(contenido);

                $("#mdlEditar_idestado").val(idestado);
            },
            error: function (resultado) {
                $("#mensaje").html(resultado.responseJSON.message);
            }
        });
    }
    function Registrar(){
        $.ajax({
            url: "{{ url('/')}}/api/usuarios/agregar",
            type: "POST",
            data: {
                nombres     : $("#nombres").val(),
                apellido1   : $("#apellido1").val(),
                apellido2   : $("#apellido2").val(),
                idtipodoc   : $("#idtipodoc").val(),
                numdoc      : $("#numdoc").val(),
                correo      : $("#correo").val(),
                usuario     : $("#usuario").val(),
            },
            headers: { authorization: "{{ session()->get('token') }}" },
            success: function (resultado) {
                Swal.fire({
                    title: 'Realizado',
                    html: 'Se registró correctamente. La contraseña es: <strong>' + resultado.password + '</strong>. Si el susario desea puede cambiar la contraseña al iniciar sesion ingresando a la opción: <i>"¿Olvidaste tu contraseña?"</i>',
                    type: 'success',
                    confirmButtonText: 'Aceptar'
                }).then((result) => {
                    if (result.value) {
                        $("#mdlAgregar").modal('hide');
                        CargaData();
                    }
                })
            },
            error: function (resultado) {
                Swal.fire('Error', resultado.responseJSON.mensaje, 'error');
            }
        });
    }
    function abre_mdlAgregar(){
        CargaTipodoc();
        $("#mdlAgregar").modal('show');
    }
    function abre_mdlEditar(idusuario){
        $("#mdlEditar_guardar").attr('idusuario', idusuario);
        $("#mdlEditar").modal('show');
        
        CargaUsuario(idusuario);
    }
    function CargaData(){
        $("#tblData").html('<tr><td colspan="7"><div class="text-center"><span class="spinner-border"></span></div></td></tr>');
        $.ajax({
            url: "{{ url('/')}}/api/usuarios",
            type: "GET",
            headers: { authorization: "{{ session()->get('token') }}" },
            success: function (resultado) {
                var contenido = "";
                $.each(resultado.data, function(i, item){
                    contenido += '<tr>';
                    
                    contenido += '<td>' + item.usuario + '</td>';
                    contenido += '<td>' + item.correo + '</td>';
                    contenido += '<td>' + item.nombres + ' ' + item.apellido1 + ' ' + item.apellido2 + '</td>';
                    contenido += '<td>' + item.fecregistro + '</td>';
                    contenido += '<td>' + item.registrador + '</td>';
                    contenido += '<td><strong>' + item.estado + '</td>';

                    contenido += '<td class="text-center">';
                    contenido += '<a href="#" class="btn btn-sm btn-light" onclick="abre_mdlEditar(' + item.id + ')"><i class="fas fa-pen"></i></a>';
                    contenido += '<a href="#" class="btn btn-sm btn-light" onclick="abre_mdlEliminar(' + item.id + ')"><i class="fas fa-trash"></i></a>';
                    contenido += '</td>';

                    contenido += '</tr>';
                });

                $("#tblData").html(contenido);
            },
            error: function (resultado) {
                $("#mensaje").html(resultado.responseJSON.message);
            }
        });
    }
    function CargaUsuario(idusuario){
        $("#mdlEditar_nombres").val("");
        $("#mdlEditar_apellido1").val("");
        $("#mdlEditar_apellido2").val("");
        $("#mdlEditar_idtipodoc").val("");
        $("#mdlEditar_numdoc").val("");
        $("#mdlEditar_correo").val("");
        $("#mdlEditar_usuario").val("");

        $.ajax({
            url: "{{ url('/')}}/api/usuarios/" + idusuario,
            type: "GET",
            headers: { authorization: "{{ session()->get('token') }}" },
            success: function (resultado) {                
                $("#mdlEditar_nombres").val(resultado.data[0].nombres);
                $("#mdlEditar_apellido1").val(resultado.data[0].apellido1);
                $("#mdlEditar_apellido2").val(resultado.data[0].apellido2);
                $("#mdlEditar_idtipodoc").val(resultado.data[0].idtipodoc);
                $("#mdlEditar_numdoc").val(resultado.data[0].numdoc);
                $("#mdlEditar_correo").val(resultado.data[0].correo);
                $("#mdlEditar_usuario").val(resultado.data[0].usuario);

                CargaTipodoc(resultado.data[0].idtipodoc);
                CargaEstado(resultado.data[0].idestado);
                
                
            },
            error: function (resultado) {
                $("#mensaje").html(resultado.responseJSON.message);
            }
        });
    }
    function Guardar(){
        $.ajax({
            url: "{{ url('/')}}/api/usuarios/modificar",
            type: "POST",
            data: {
                id          : $("#mdlEditar_guardar").attr('idusuario'),
                nombres     : $("#mdlEditar_nombres").val(),
                apellido1   : $("#mdlEditar_apellido1").val(),
                apellido2   : $("#mdlEditar_apellido2").val(),
                idtipodoc   : $("#mdlEditar_idtipodoc").val(),
                numdoc      : $("#mdlEditar_numdoc").val(),
                correo      : $("#mdlEditar_correo").val(),
                idestado    : $("#mdlEditar_idestado").val(),
            },
            headers: { authorization: "{{ session()->get('token') }}" },
            success: function (resultado) {
                Swal.fire({
                    title: 'Realizado',
                    html: 'Se registró correctamente.',
                    type: 'success',
                    confirmButtonText: 'Aceptar'
                }).then((result) => {
                    if (result.value) {
                        $("#mdlEditar").modal('hide');
                        CargaData();
                    }
                })
            },
            error: function (resultado) {
                Swal.fire('Error', resultado.responseJSON.mensaje, 'error');
            }
        });
    }
    function abre_mdlEliminar(idusuario){
        $("#mdlEliminar_eliminar").attr('idusuario', idusuario);
        $("#mdlEliminar").modal('show');
    }
    function Eliminar(){
        var id = $("#mdlEliminar_eliminar").attr('idusuario');
        $.ajax({
            url: "{{ url('/')}}/api/usuarios/eliminar",
            type: "POST",
            data: { id : id },
            headers: { authorization: "{{ session()->get('token') }}" },
            success: function (resultado) {
                Swal.fire({
                    title: 'Realizado',
                    html: 'El registro fué eliminado',
                    type: 'success',
                    confirmButtonText: 'Aceptar'
                }).then((result) => {
                    if (result.value) {
                        $("#mdlEliminar").modal('hide');
                        CargaData();
                    }
                })
            },
            error: function (resultado) {
                Swal.fire('Error', resultado.responseJSON.mensaje, 'error');
            }
        });
    }
</script>

@endsection