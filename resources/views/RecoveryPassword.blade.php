<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Main CSS-->
        <link rel="stylesheet" type="text/css" href="{{ url('/') }}/templates/css/main.css">
		<!-- Font-icon css-->
		<link rel="stylesheet" type="text/css" href="{{ url('/') }}/templates/fontawesome/css/all.css">
		<style>
			.cover{
				background-color: #e7e7e7 !important;
			}
			.material-half-bg{
				background-color: #dc0613 !important;
			}
			.btn-primary{
				background-color: #dc0613 !important;
				border-color: #dc0613 !important;
			}
		</style>
		<title>Klar</title>
	</head>
	<body>
		<div class="container mt-5">
			<div class="row">
				<div class="col-md-12">
					<p>Para recuperara sus credenciales de acceso, usted debe solicitar cambiar de contraseña ingresando el usuario en la caja de texto. El sistema le enviará un código de recuperacion al correo con el cual podrá modificar su contraseña.</p>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label">USUARIO</label>
						<input id="usuario" class="form-control" type="text" placeholder="Usuario" autofocus>
					</div>

					<div class="form-group btn-container">
						<button class="btn btn-primary btn-block" onclick="GetRecoveryCode()">Generar código de recuperacion</button>
					</div>
				</div>
			</div>
		</div>




		<!-- The Modal -->
		<div class="modal fade" id="mdlRecuperar">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">

				<!-- Modal Header -->
				<div class="modal-header">
					<h4 class="modal-title">Codigo de Recuperación</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

				<!-- Modal body -->
				<div class="modal-body">
					<span id="mdlRecuperar-info"></span>
					<input id="codigorecuperacion" class="form-control text-center" style="font-size: 30px;" type="number" placeholder="Código" autofocus>
				</div>

				<!-- Modal footer -->
				<div class="modal-footer">
					<a href="#" class="btn btn-danger"  onclick="ValidarCodigo()">Recuperar</a>
				</div>

				</div>
			</div>
		</div>





	</body>


    <!-- Essential javascripts for application to work-->
	<script src="{{ url('/') }}/templates/js/jquery-3.2.1.min.js"></script>
	<script src="{{ url('/') }}/templates/js/popper.min.js"></script>
	<script src="{{ url('/') }}/templates/js/bootstrap.min.js"></script>
	<script src="{{ url('/') }}/templates/js/main.js"></script>
	<!-- The javascript plugin to display page loading on top-->
	<script src="{{ url('/') }}/templates/js/plugins/pace.min.js"></script>
        
    <script>
        $( document ).ready(function() {
			// Recuperar();
        });
		function GetRecoveryCode(){
			$.ajax({
                url: "{{ url('/')}}/admin/login/obtener-codigo",
                type: "POST",
                data: {
					usuario : $("#usuario").val()
				},
                success: function (resultado) {
                    console.log(resultado);
					if(resultado.status == 'success'){
						$("#mdlRecuperar-info").html(resultado.mensaje);
						$("#mdlRecuperar").modal("show");
					}
                },
                error: function (resultado) {
					console.log(resultado);
                    //$("#mensaje").html(resultado.responseJSON.message);
                }
            });
		}
		function ValidarCodigo(){
			$.ajax({
                url: "{{ url('/')}}/admin/login/validar-codigo",
                type: "POST",
                data: {
					usuario : $("#usuario").val(),
					codigo : $("#codigorecuperacion").val()
				},
                success: function (resultado) {
                    console.log(resultado);
					if(resultado.respuesta == 1){
						console.log(resultado);
						window.location.href = "{{ url('/') }}/admin/open-change-pasword/" + $("#usuario").val();
					}
                },
                error: function (resultado) {
					console.log(resultado);
                    //$("#mensaje").html(resultado.responseJSON.message);
                }
            });
		}
    </script>
</html>

