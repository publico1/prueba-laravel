
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Patio Panorama | {{ $error }} Error</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">


    <link href="{{ url('/') }}css/animate.css" rel="stylesheet">
    <link href="{{ url('/') }}css/style.css" rel="stylesheet">
    
</head>

<body class="gray-bg">


    <div class="middle-box text-center animated fadeInDown">
        <h1>{{ $error }}</h1>
        <h3 class="font-bold">{{ $error_titulo }}</h3>

        <div class="error-desc">
            {{ $error_descripcion }}
        </div>
        <p><a class="btn btn-primary" href="{{ url('/') }}/admin">Iniciar sesion</a></p>
    </div>

    <!-- Mainly scripts -->
    <script src="{{ url('/') }}/templates/master/js/jquery-3.1.1.min.js"></script>
    <script src="{{ url('/') }}/templates/master/js/bootstrap.min.js"></script>

</body>

</html>