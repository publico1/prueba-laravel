<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Main CSS-->
        <link rel="stylesheet" type="text/css" href="{{ url('/') }}/templates/css/main.css">
		<!-- Font-icon css-->
		<link rel="stylesheet" type="text/css" href="{{ url('/') }}/templates/fontawesome/css/all.css">
		<style>
			.cover{
				background-color: #e7e7e7 !important;
			}
			.material-half-bg{
				background-color: #ffffff !important;
			}
			.btn-primary{
				background-color: #dc0613 !important;
				border-color: #dc0613 !important;
			}
		</style>
		<title>Cambiar Password</title>
	</head>
	<body>
		<section class="material-half-bg">
			<div class="cover"></div>
		</section>
		<section class="login-content">
			<div class="logo">
				<h1><img src="{{ url('/') }}/front/images/logo.png" alt="" style="height:60px"></h1>
			</div>
			<div class="login-box">
				<div class="login-form" id="frmLogin" autocomplete="off">
					<h3 class="login-head">Cambiar contraseña del usuario <u>{{ Request()->usuario }}</u></h3>
					<div class="form-group">
						<label class="control-label">NUEVA CONTRASEÑA</label>
						<input id="password_1" class="form-control" type="password" placeholder="Password" autocomplete="off">
                    </div>
					<div class="form-group">
						<label class="control-label">REPITA LA NUEVA CONTRASEÑA</label>
						<input id="password_2" class="form-control" type="password" placeholder="Password">
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-12"><span class="badge bg-danger text-light" id="mensaje"></span></div>
                    </div>
					
					<div class="form-group btn-container">
						<button class="btn btn-primary btn-block" onclick="ChangePassword()">CAMBIAR CONTRASEÑA</button>
					</div>
				</div>
			</div>
		</section>

		<!-- The Modal -->
		<div class="modal fade" id="mdlMensaje">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">

					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title">Codigo de Recuperación</h4>
					</div>

					<!-- Modal body -->
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12" id="mdlMensaje-info"><?php /* JQuery */ ?><div>
						</div>
					</div>

				</div>
			</div>
		</div>



		<!-- Essential javascripts for application to work-->
		<script src="{{ url('/') }}/templates/js/jquery-3.2.1.min.js"></script>
		<script src="{{ url('/') }}/templates/js/popper.min.js"></script>
		<script src="{{ url('/') }}/templates/js/bootstrap.min.js"></script>
		<script src="{{ url('/') }}/templates/js/main.js"></script>
		<!-- The javascript plugin to display page loading on top-->
		<script src="{{ url('/') }}/templates/js/plugins/pace.min.js"></script>
		<script type="text/javascript">
			// Login Page Flipbox control
			$('.login-content [data-toggle="flip"]').click(function() {
				$('.login-box').toggleClass('flipped');
				return false;
			});
        </script>
        
        <script>
            $( document ).ready(function() {                
                //
            });
			function ChangePassword(){
				$.ajax({
                url: "{{ url('/')}}/admin/login/cambiar-contrasenia",
                type: "POST",
                data: {
					usuario : "{{ Request()->usuario }}",
					password_1 : $("#password_1").val(),
					password_2 : $("#password_2").val()
				},
                success: function (resultado) {
                    console.log(resultado);
					if(resultado.status == 'success'){
						//{backdrop: 'static', keyboard: false}
						$("#mdlMensaje-info").html('<p>' + resultado.mensaje + '</p><a class="btn btn-sm btn-primary" href="{{ url("/") }}/admin">Ir al login</a>');
						$("#mdlMensaje").modal({backdrop: 'static', keyboard: false});
						// window.location.href = "{{ url('/') }}/admin";
					}
                },
                error: function (resultado) {
					$("#mdlMensaje-info").html('<p>' + resultado.responseJSON.mensaje + '</p><button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Regresar</button>');
					$("#mdlMensaje").modal({backdrop: 'static', keyboard: false});
					console.log(resultado);
                    //$("#mensaje").html(resultado.responseJSON.message);
                }
            });
			}
        </script>
	</body>
</html>

