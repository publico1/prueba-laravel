<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Star Admin Free Bootstrap Admin Dashboard Template</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{ url('/') }}/template/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="{{ url('/') }}/template/vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="{{ url('/') }}/template/vendors/css/vendor.bundle.addons.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{ url('/') }}/template/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{ url('/') }}/template/images/favicon.png" />
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
      <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one" style="background: url({{ url('/') }}/images/fondo.jpg);background-size: cover">
        <div class="row w-100">
          <div class="col-lg-4 mx-auto">
            <div class="auto-form-wrapper">
              <form id="frmLogin">
                <div class="form-group">
                <label class="label">Usuario</label>
                <div class="input-group">
                  <!-- <input type="text" class="form-control" placeholder="Username"> -->
                  <input name="usuario" class="form-control" type="text" placeholder="Usuario" autofocus>
                  <div class="input-group-append">
                  <span class="input-group-text">
                    <i class="mdi mdi-check-circle-outline"></i>
                  </span>
                  </div>
                </div>
                </div>
                <div class="form-group">
                  <label class="label">Contraseña</label>
                  <div class="input-group">
                    <input name="password" class="form-control" type="password" placeholder="*********">
                    <div class="input-group-append">
                    <span class="input-group-text">
                      <i class="mdi mdi-check-circle-outline"></i>
                    </span>
                    </div>
                  </div>
                </div>
                
                <div class="row mb-2">
                  <div class="col-md-12"><span class="badge bg-danger text-light" style="min-width:0px;" id="mensaje"></span></div>
                </div>

                <div class="form-group">
                  <button class="btn btn-primary submit-btn btn-block">Ingresar</button>
                </div>
                <div class="form-group d-flex">
                  <a href="#" class="text-small forgot-password text-black">¿Olvidaste tu contraseña?</a>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="{{ url('/') }}/template/vendors/js/vendor.bundle.base.js"></script>
  <script src="{{ url('/') }}/template/vendors/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="{{ url('/') }}/template/js/off-canvas.js"></script>
  <script src="{{ url('/') }}/template/js/misc.js"></script>
  <!-- endinject -->


  	<script>
        $( document ).ready(function() {
            $("#frmLogin").on('submit', function (e){
                e.preventDefault();
                $("#mensaje").html('');
                $.ajax({
                    processData: false,
                    contentType: false,
                    url: "{{ url('/')}}/admin/login",
                    type: "POST",
                    data: new FormData(this),
                    success: function (resultado) {
                        if(resultado.acceso){
                            location.reload();
                        }
                    },
                    error: function (resultado) {
                        $("#mensaje").html(resultado.responseJSON.message);
                    }
                });
            });
        });
    </script>
</body>

</html>