<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Footer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('footer', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('nombre');            
            $table->string('html', 1000)->nullable();
            $table->string('copyright', 1000)->nullable();
            $table->string('enlaces', 1000)->nullable();
            $table->integer('idimagen')->nullable();
            $table->string('redessociales', 1000)->nullable();
            $table->integer('idestado');
            $table->timestamp('fecregistro');
            $table->integer('usuregistro');

            $table->foreign('idestado')->references('id')->on('estado');
            $table->foreign('usuregistro')->references('id')->on('usuario');
            $table->foreign('idimagen')->references('id')->on('imagen');

            $table->engine = 'InnoDB';
        });

        DB::table('footer')->insert(
            array(
                'id' => '1',
                'nombre' => 'Footer Principal',
                'html' => '<p>Av. Circunvalación del Golf Los Incas 134,<br/>Santiago de Surco, Lima - Perú</p><a href="tel:+51924350729">01 924 350 729</a><a href="mailto:info@panorama.pe">info@panorama.pe</a>',
                'copyright' => '<p>© 2019 Patio Panorama. Todos los Derechos Reservados.</p>',
                'enlaces' => '[{"id":1,"texto":"Somos","url":"#"},{"id":2,"texto":"Locales","url":"#"},{"id":3,"texto":"Cines","url":"#"},{"id":4,"texto":"Actividades","url":"#"},{"id":5,"texto":"Promociones","url":"#"},{"id":6,"texto":"Blog","url":"#"},{"id":7,"texto":"Contactenos","url":"#"}]',
                'idimagen' => '20',
                'redessociales' => '[{"idimagen":"12","nombre":"Facebook","url":"#"},{"idimagen":"13","nombre":"Instagram","url":"#"}]',
                'idestado' => '1',
                'fecregistro' => '2019-09-26 11:22:54',
                'usuregistro' => '1'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
