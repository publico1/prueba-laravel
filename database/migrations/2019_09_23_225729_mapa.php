<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Mapa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mapa', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('nombre');
            $table->integer('idimagen');
            $table->string('html', 1000)->nullable();
            $table->integer('idestado');
            $table->timestamp('fecregistro');
            $table->integer('usuregistro');

            $table->foreign('idestado')->references('id')->on('estado');
            $table->foreign('idimagen')->references('id')->on('imagen');
            $table->foreign('usuregistro')->references('id')->on('usuario');

            $table->engine = 'InnoDB';
        });

        DB::table('mapa')->insert(
            array(
                'id' => '1',
                'nombre' => 'Ubicación',
                'idimagen' => '16',
                'html' => '<p>Av. Circunvalación del Golf los Incas 134<br/>Santiago de Surco, Lima - Perú</p><p>10:00am - 11:00 pm</p><a href="tel:+51924350729">924 350 729</a><p>Abrir en<a href="https://www.waze.com/es/livemap/directions/peru/municipalidad-metropolitana-de-lima/santiago-de-surco/patio-panorama?place=ChIJiwUBS6vHBZERltg239MP6fM" target="_blank">{@agencia_image:24092019_165919.png}</a></p>',
                'idestado' => '1',
                'fecregistro' => '2019-09-24 15:26:22',
                'usuregistro' => '1'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
