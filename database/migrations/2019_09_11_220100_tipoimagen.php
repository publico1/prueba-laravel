<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tipoimagen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipoimagen', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('nombre');

            $table->engine = 'InnoDB';
        });

        DB::table('tipoimagen')->insert(
            array(
                [
                    'id' => '1',
                    'nombre' => 'Variado'
                ],[
                    'id' => '2',
                    'nombre' => 'Fondo L'
                ],[
                    'id' => '3',
                    'nombre' => 'Fondo M'
                ],[
                    'id' => '4',
                    'nombre' => 'Fondo S'
                ],[
                    'id' => '5',
                    'nombre' => 'Icono L'
                ],[
                    'id' => '6',
                    'nombre' => 'Icono M'
                ],[
                    'id' => '7',
                    'nombre' => 'Icono S'
                ],
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
