<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Rol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rol', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('nombre');
            $table->integer('idestado');

            $table->foreign('idestado')->references('id')->on('estado');

            $table->engine = 'InnoDB';
        });

        DB::table('rol')->insert(
            array(
                [
                    'id' => '1',
                    'nombre' => 'Super Administrador',
                    'idestado' => '1'
                ],[
                    'id' => '2',
                    'nombre' => 'Administrador',
                    'idestado' => '1'
                ]
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
