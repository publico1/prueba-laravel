<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Grilla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grilla', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('nombre', 50);
            $table->integer('idestado');
            $table->integer('idtipogrilla');
            $table->timestamp('fecregistro');
            $table->integer('usuregistro');

            $table->foreign('idestado')->references('id')->on('estado');
            $table->foreign('idtipogrilla')->references('id')->on('tipogrilla');
            $table->foreign('usuregistro')->references('id')->on('usuario');

            $table->engine = 'InnoDB';
        });

        DB::table('grilla')->insert(
            array(
                [
                    'id' => '1',
                    'nombre' => 'Grilla Protada',
                    'idestado' => '1',
                    'idtipogrilla' => '1',
                    'fecregistro' => '2019-09-24 15:26:22',
                    'usuregistro' => '1'
                ],[
                    'id' => '2',
                    'nombre' => 'Grilla Locales',
                    'idestado' => '1',
                    'idtipogrilla' => '1',
                    'fecregistro' => '2019-09-24 15:26:22',
                    'usuregistro' => '1'
                ],[
                    'id' => '3',
                    'nombre' => 'Grilla Cine',
                    'idestado' => '1',
                    'idtipogrilla' => '1',
                    'fecregistro' => '2019-09-24 15:26:22',
                    'usuregistro' => '1'
                ]            
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
