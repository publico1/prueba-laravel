<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GrillaImagen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grilla_imagen', function (Blueprint $table) {
            $table->integer('idgrilla');
            $table->integer('idimagen');
            $table->string('titulo');
            $table->string('html', 1000)->nullable();
            $table->integer('orden');

            $table->foreign('idgrilla')->references('id')->on('grilla');
            $table->foreign('idimagen')->references('id')->on('imagen');

            $table->primary(array('idgrilla', 'idimagen'));

            $table->engine = 'InnoDB';
        });

        DB::table('grilla_imagen')->insert(
            array(
                [
                    'idgrilla' => '1',
                    'idimagen' => '4',
                    'titulo' => 'ESTACIONAMIENTOS',
                    'html' => '<p>300 estacionamientos solo para clientes de Patio Panorama. 4 sótanos vigilados.</p>',
                    'orden' => '1'
                ],[
                    'idgrilla' => '1',
                    'idimagen' => '5',
                    'titulo' => 'SALAS DE CINE',
                    'html' => '<p>10 modernas amplias de cine para que puedas disfrutar en familia.</p>',
                    'orden' => '2'
                ],[
                    'idgrilla' => '1',
                    'idimagen' => '6',
                    'titulo' => 'SERVICIOS',
                    'html' => '<p>Servicios de cajeros, farmacia, peluquería y lavanderia para complementar tu día a día.</p>',
                    'orden' => '3'
                ],[
                    'idgrilla' => '1',
                    'idimagen' => '7',
                    'titulo' => 'SUPERMERCADO',
                    'html' => '<p>Contamos con una tienda Wong en nuestro sótano, Ideal para comprar lo que puedes necesitar rápido.</p>',
                    'orden' => '4'
                ],[
                    'idgrilla' => '1',
                    'idimagen' => '8',
                    'titulo' => 'GIMNASIO',
                    'html' => '<p>Contamos con un amplio y moderno Gimnasio, con máquinas y clases.</p>',
                    'orden' => '5'
                ],[
                    'idgrilla' => '1',
                    'idimagen' => '9',
                    'titulo' => 'TERRAZA',
                    'html' => '<p>Una moder terraza centrar en la cual realizamos distintos eventos.</p>',
                    'orden' => '6'
                ],[
                    'idgrilla' => '1',
                    'idimagen' => '10',
                    'titulo' => 'RESTAURANTES',
                    'html' => '<p>Los restaurantes más importantes de la ciudad y una amplia variedad.</p>',
                    'orden' => '7'
                ],[
                    'idgrilla' => '1',
                    'idimagen' => '11',
                    'titulo' => 'OFICINAS',
                    'html' => '<p>Patio Panorama se complementa con 03 torres modernas oficinas.</p>',
                    'orden' => '8'
                ],[
                    'idgrilla' => '2',
                    'idimagen' => '14',
                    'titulo' => '{@agencia_image:30092019_163135.png}',
                    'html' => '<p>Somos un Fast Casual con el propósito de ofrecer una verdadera alimentación nutritiva y saludable a nuestra comunidad.</p><a href="inner-local.html">VER MÁS</a>',
                    'orden' => '1'
                ],[
                    'idgrilla' => '2',
                    'idimagen' => '23',
                    'titulo' => '{@agencia_image:30092019_163135.png}',
                    'html' => '<p>Somos un Fast Casual con el propósito de ofrecer una verdadera alimentación nutritiva y saludable a nuestra comunidad.</p><a href="inner-local.html">VER MÁS</a>',
                    'orden' => '2'
                ],[
                    'idgrilla' => '2',
                    'idimagen' => '24',
                    'titulo' => '{@agencia_image:30092019_163135.png}',
                    'html' => '<p>Somos un Fast Casual con el propósito de ofrecer una verdadera alimentación nutritiva y saludable a nuestra comunidad.</p><a href="inner-local.html">VER MÁS</a>',
                    'orden' => '3'
                ],[
                    'idgrilla' => '2',
                    'idimagen' => '25',
                    'titulo' => '{@agencia_image:30092019_163135.png}',
                    'html' => '<p>Somos un Fast Casual con el propósito de ofrecer una verdadera alimentación nutritiva y saludable a nuestra comunidad.</p><a href="inner-local.html">VER MÁS</a>',
                    'orden' => '4'
                ],[
                    'idgrilla' => '2',
                    'idimagen' => '26',
                    'titulo' => '{@agencia_image:30092019_163135.png}',
                    'html' => '<p>Somos un Fast Casual con el propósito de ofrecer una verdadera alimentación nutritiva y saludable a nuestra comunidad.</p><a href="inner-local.html">VER MÁS</a>',
                    'orden' => '5'
                ],[
                    'idgrilla' => '2',
                    'idimagen' => '27',
                    'titulo' => '{@agencia_image:30092019_163135.png}',
                    'html' => '<p>Somos un Fast Casual con el propósito de ofrecer una verdadera alimentación nutritiva y saludable a nuestra comunidad.</p><a href="inner-local.html">VER MÁS</a>',
                    'orden' => '6'
                ],[
                    'idgrilla' => '2',
                    'idimagen' => '28',
                    'titulo' => '{@agencia_image:30092019_163135.png}',
                    'html' => '<p>Somos un Fast Casual con el propósito de ofrecer una verdadera alimentación nutritiva y saludable a nuestra comunidad.</p><a href="inner-local.html">VER MÁS</a>',
                    'orden' => '7'
                ],[
                    'idgrilla' => '2',
                    'idimagen' => '29',
                    'titulo' => '{@agencia_image:30092019_163135.png}',
                    'html' => '<p>Somos un Fast Casual con el propósito de ofrecer una verdadera alimentación nutritiva y saludable a nuestra comunidad.</p><a href="inner-local.html">VER MÁS</a>',
                    'orden' => '8'
                ],[
                    'idgrilla' => '2',
                    'idimagen' => '30',
                    'titulo' => '{@agencia_image:30092019_163135.png}',
                    'html' => '<p>Somos un Fast Casual con el propósito de ofrecer una verdadera alimentación nutritiva y saludable a nuestra comunidad.</p><a href="inner-local.html">VER MÁS</a>',
                    'orden' => '9'
                ],[
                    'idgrilla' => '2',
                    'idimagen' => '31',
                    'titulo' => '{@agencia_image:30092019_163135.png}',
                    'html' => '<p>Somos un Fast Casual con el propósito de ofrecer una verdadera alimentación nutritiva y saludable a nuestra comunidad.</p><a href="inner-local.html">VER MÁS</a>',
                    'orden' => '10'
                ],[
                    'idgrilla' => '2',
                    'idimagen' => '32',
                    'titulo' => '{@agencia_image:30092019_163135.png}',
                    'html' => '<p>Somos un Fast Casual con el propósito de ofrecer una verdadera alimentación nutritiva y saludable a nuestra comunidad.</p><a href="inner-local.html">VER MÁS</a>',
                    'orden' => '11'
                ],[
                    'idgrilla' => '2',
                    'idimagen' => '33',
                    'titulo' => '{@agencia_image:30092019_163135.png}',
                    'html' => '<p>Somos un Fast Casual con el propósito de ofrecer una verdadera alimentación nutritiva y saludable a nuestra comunidad.</p><a href="inner-local.html">VER MÁS</a>',
                    'orden' => '12'
                ],[
                    'idgrilla' => '2',
                    'idimagen' => '34',
                    'titulo' => '{@agencia_image:30092019_163135.png}',
                    'html' => '<p>Somos un Fast Casual con el propósito de ofrecer una verdadera alimentación nutritiva y saludable a nuestra comunidad.</p><a href="inner-local.html">VER MÁS</a>',
                    'orden' => '13'
                ],[
                    'idgrilla' => '3',
                    'idimagen' => '34',
                    'titulo' => '',
                    'html' => '<p>Salas tipo estadio con perfecta visibilidad.</p>',
                    'orden' => '13'
                ],[
                    'idgrilla' => '3',
                    'idimagen' => '34',
                    'titulo' => '',
                    'html' => '<p>Sonido Dolby Surround Digital en todas sus salas.</p>',
                    'orden' => '13'
                ],[
                    'idgrilla' => '3',
                    'idimagen' => '34',
                    'titulo' => '',
                    'html' => '<p>Pantallas curvas y flotantes.</p>',
                    'orden' => '13'
                ]
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
