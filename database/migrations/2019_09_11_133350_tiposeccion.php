<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tiposeccion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tiposeccion', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('nombre');
            $table->integer('idestado');

            $table->foreign('idestado')->references('id')->on('estado');

            $table->engine = 'InnoDB';
        });

        DB::table('tiposeccion')->insert(
            array(
                [
                    'id' => '1',
                    'nombre' => 'HTML',
                    'idestado' => '1'
                ]
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
