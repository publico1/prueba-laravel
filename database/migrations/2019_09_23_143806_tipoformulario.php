<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tipoformulario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipoformulario', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('nombre', 50);

            $table->engine = 'InnoDB';
        });

        DB::table('tipoformulario')->insert(
            array(
                [
                    'id' => '1',
                    'nombre' => 'Email'
                ]
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
