<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Seperador extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('separador', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('nombre');
            $table->integer('idestado');
            $table->string('html', 1000)->nullable();
            $table->timestamp('fecregistro');
            $table->integer('usuregistro');
                      

            $table->foreign('idestado')->references('id')->on('estado');
            $table->foreign('usuregistro')->references('id')->on('usuario');

            $table->engine = 'InnoDB';
        });

        DB::table('separador')->insert(
            array(
                [
                    'id' => '1',
                    'nombre' => 'CONTENIDO SUPER INTERESANTE',
                    'html' => '<h5>CONTENIDO SUPER INTERESANTE<h5>',
                    'idestado' => '1',
                    'fecregistro' => '2019-09-24 15:26:22',
                    'usuregistro' => '1'
                ],[
                    'id' => '2',
                    'nombre' => 'TAMBIÉN TE PODRÍA INTERESAR',
                    'html' => '<h5>TAMBIÉN TE PODRÍA INTERESAR<h5>',
                    'idestado' => '1',
                    'fecregistro' => '2019-09-24 15:26:22',
                    'usuregistro' => '1'
                ]
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
