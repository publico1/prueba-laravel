<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pagina extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagina', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('nombre', 100);
            $table->integer('idestado');
            $table->string('url', 200);
            $table->string('colorfondo', 10);
            $table->string('css', 200)->nullable();
            $table->string('js', 200)->nullable();
            $table->timestamp('fecregistro');
            $table->string('maquetado', 6000)->nullable();
            $table->integer('usuregistro');

            $table->foreign('idestado')->references('id')->on('estado');

            $table->engine = 'InnoDB';
        });

        DB::table('pagina')->insert(
            array(
                [
                    'id' => '1',
                    'nombre' => 'HOME',
                    'url' => '/',
                    'colorfondo' => '#e6e6e6',
                    'css' => '',
                    'js' => '',
                    'fecregistro' => '2019-09-10 00:00:00',
                    'idestado' => '1',
                    'maquetado' => '[{"tipo":"menu","tabla":"menu","idtabla":"1"},{"idtabla":"1","tabla":"slider","tipo":"slider"},{"idtabla":"1","tabla":"grilla","tipo":"grilla"},{"idtabla":"2","tabla":"slider","tipo":"slider"},{"idtabla":"1","tabla":"mapa","tipo":"mapa"},{"idtabla":"1","tabla":"formulario","tipo":"formulario"},{"idtabla":"1","tabla":"separador","tipo":"separador"},{"idtabla":"3","tabla":"slider","tipo":"slider"},{"idtabla":"1","tabla":"footer","tipo":"footer"}]',
                    'usuregistro' => '1'
                ],[
                    'id' => '2',
                    'nombre' => 'SOMOS',
                    'url' => '/somos',
                    'colorfondo' => '#e6e6e6',
                    'css' => '',
                    'js' => '',
                    'fecregistro' => '2019-09-10 00:00:00',
                    'idestado' => '1',
                    'maquetado' => '[{"tipo":"menu","tabla":"menu","idtabla":"1"},{"idtabla":"1","tabla":"slider","tipo":"slider"},{"idtabla":"1","tabla":"grilla","tipo":"grilla"}]',
                    'usuregistro' => '1'
                ],[
                    'id' => '3',
                    'nombre' => 'LOCALES',
                    'url' => '/locales',
                    'colorfondo' => '#414141',
                    'css' => '',
                    'js' => '',
                    'fecregistro' => '2019-09-10 00:00:00',
                    'idestado' => '1',
                    'maquetado' => '[{"tipo":"menu","tabla":"menu","idtabla":"1"},{"idtabla":"4","tabla":"slider","tipo":"slider"},{"idtabla":"1","tabla":"separador","tipo":"separador"},{"idtabla":"3","tabla":"slider","tipo":"slider"},{"idtabla":"1","tabla":"footer","tipo":"footer"}]',
                    'usuregistro' => '1'
                ],[
                    'id' => '4',
                    'nombre' => 'CINE',
                    'url' => '/cine',
                    'colorfondo' => '#e6e6e6',
                    'css' => '',
                    'js' => '',
                    'fecregistro' => '2019-09-10 00:00:00',
                    'idestado' => '1',
                    'maquetado' => '[]',
                    'usuregistro' => '1'
                ],[
                    'id' => '5',
                    'nombre' => 'PROMOCIONES',
                    'url' => '/promociones',
                    'colorfondo' => '#e6e6e6',
                    'css' => '',
                    'js' => '',
                    'fecregistro' => '2019-09-10 00:00:00',
                    'idestado' => '1',
                    'maquetado' => '[]',
                    'usuregistro' => '1'
                ],[
                    'id' => '6',
                    'nombre' => 'ACTIVIDADES',
                    'url' => '/actividades',
                    'colorfondo' => '#e6e6e6',
                    'css' => '',
                    'js' => '',
                    'fecregistro' => '2019-09-10 00:00:00',
                    'idestado' => '1',
                    'maquetado' => '[]',
                    'usuregistro' => '1'
                ],[
                    'id' => '7',
                    'nombre' => 'BLOG',
                    'url' => '/blog',
                    'colorfondo' => '#e6e6e6',
                    'css' => '',
                    'js' => '',
                    'fecregistro' => '2019-09-10 00:00:00',
                    'idestado' => '1',
                    'maquetado' => '[]',
                    'usuregistro' => '1'
                ],[
                    'id' => '8',
                    'nombre' => 'CONTÁCTENOS',
                    'url' => '/contactenos',
                    'colorfondo' => '#e6e6e6',
                    'css' => '',
                    'js' => '',
                    'fecregistro' => '2019-09-10 00:00:00',
                    'idestado' => '1',
                    'maquetado' => '[]',
                    'usuregistro' => '1'
                ],
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
