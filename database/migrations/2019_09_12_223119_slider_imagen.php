<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SliderImagen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slider_imagen', function (Blueprint $table) {
            $table->integer('idslider');
            $table->integer('idimagen');
            $table->integer('idmagenlogo')->nullable();
            $table->string('titulo', 1000)->nullable();
            $table->string('html', 1000)->nullable();
            $table->integer('orden');

            $table->foreign('idslider')->references('id')->on('slider');
            $table->foreign('idimagen')->references('id')->on('imagen');
            $table->foreign('idmagenlogo')->references('id')->on('imagen');
            
            $table->primary(array('idslider', 'idimagen'));

            $table->engine = 'InnoDB';
        });

        DB::table('slider_imagen')->insert(
            array(
                [
                    'idslider' => '1',
                    'idimagen' => '21',
                    'idmagenlogo' => null,
                    'titulo' => '<h4>Life Style Center</h4>',
                    'html' => '<p>Texto</p>',
                    'orden' => 1
                ],[
                    'idslider' => '1',
                    'idimagen' => '3',
                    'idmagenlogo' => null,
                    'titulo' => '<h4>Life Style Center</h4>',
                    'html' => '<p>Texto</p>',
                    'orden' => 2
                ],[
                    'idslider' => '2',
                    'idimagen' => '14',
                    'idmagenlogo' => '15',
                    'titulo' => '<h4>Comida de la selva</h4>',
                    'html' => '<br>Amaz, es el restaurante del chef Pedro Miguel<br/>Schiaffino. Amaz nos ofrece comida de la selva<br/>peruana, con toques únicos de un gran chef.</p><a href="javascript:void(0)">RESERVA AQUÍ</a>',
                    'orden' => 1
                ],[
                    'idslider' => '2',
                    'idimagen' => '2',
                    'idmagenlogo' => '10',
                    'titulo' => '<h4>Otro servicio</h4>',
                    'html' => '<br>Descripción del otro servicio',
                    'orden' => 2
                ],[
                    'idslider' => '3',
                    'idimagen' => '19',
                    'idmagenlogo' => null,
                    'titulo' => 'El fin de semana con los chicos<br/>en Patio Panorama ¿Qué puedes hacer?',
                    'html' => '<p>Lo primero y más importante es suscribiste a nuestra<br/>base de datos para que te llegue nuestra<br/>programación de eventos, pues siempre hay algo <br/>nuevo en Patio Panorama.</p><p>El fin de semana con los chicos, podrás disfrutarlo<br/>mucho, por ejemplo, puedes venir con ellos a jugar en<br/>nuestro Patio Kids, en donde descubriran el mundo de<br/>Artesco</p><a href="javascript:void(0)">LEER +</a>',
                    'orden' => 1
                ],[
                    'idslider' => '3',
                    'idimagen' => '16',
                    'idmagenlogo' => null,
                    'titulo' => 'El fin de semana con los chicos<br/>en Patio Panorama ¿Qué puedes hacer?',
                    'html' => '<p>Lo primero y más importante es suscribiste a nuestra<br/>base de datos para que te llegue nuestra<br/>programación de eventos, pues siempre hay algo <br/>nuevo en Patio Panorama.</p><p>El fin de semana con los chicos, podrás disfrutarlo<br/>mucho, por ejemplo, puedes venir con ellos a jugar en<br/>nuestro Patio Kids, en donde descubriran el mundo de<br/>Artesco</p><a href="javascript:void(0)">LEER +</a>',
                    'orden' => 2
                ],[
                    'idslider' => '4',
                    'idimagen' => '22',
                    'idmagenlogo' => null,
                    'titulo' => '<h4>Descubre lo que tenemos</h4>',
                    'html' => '<p>+ Lorem ipsum dolor sit amet consectetur adipiscing<br>elit laoreet blandit maecenas, morbi rutrum habitasse<br>aliquam quam eros torquent in.</p>',
                    'orden' => 1
                ],[
                    'idslider' => '5',
                    'idimagen' => '36',
                    'idmagenlogo' => null,
                    'titulo' => 'Cine',
                    'html' => '<p>+ Contamos con la mejor alternativa de cines por la zona. Gracias a las instalaciones de multicines UVK Premium en nuestro segundo piso, disfruta de 10 modernas salas que cuentan con:</p>',
                    'orden' => 1
                ]
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
