<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Innerlocal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('innerlocal', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('nombre');            
            $table->string('titulo', 1000)->nullable();
            $table->string('html', 3000)->nullable();
            $table->string('imagenes', 1000)->nullable();
            $table->integer('idestado');
            $table->timestamp('fecregistro');
            $table->integer('usuregistro');

            $table->foreign('idestado')->references('id')->on('estado');
            $table->foreign('usuregistro')->references('id')->on('usuario');
            $table->foreign('idimagen')->references('id')->on('imagen');

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
