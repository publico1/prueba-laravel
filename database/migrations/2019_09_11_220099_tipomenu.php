<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tipomenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipomenu', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('nombre');

            $table->engine = 'InnoDB';
        });

        DB::table('tipomenu')->insert(
            array(
                [
                    'id' => '1',
                    'nombre' => 'Primario'
                ],[
                    'id' => '2',
                    'nombre' => 'Secundario'
                ],
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
