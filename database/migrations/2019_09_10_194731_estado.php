<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Estado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estado', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('nombre');
            $table->string('siglas');

            $table->engine = 'InnoDB';
        });

        DB::table('estado')->insert(
            array(
                [
                    'id' => '0',
                    'nombre' => 'Eliminado',
                    'siglas' => 'ELI'
                ],
                [
                    'id' => '1',
                    'nombre' => 'Activo',
                    'siglas' => 'ACT'
                ],
                [
                    'id' => '2',
                    'nombre' => 'Inactivo',
                    'siglas' => 'INA'
                ]
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
