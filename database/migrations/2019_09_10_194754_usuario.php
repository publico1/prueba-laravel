<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Usuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('usuario');
            $table->string('password', 200);
            $table->string('numdoc');
            $table->integer('idtipodoc');
            $table->string('apellido1');
            $table->string('apellido2');
            $table->string('nombres');
            $table->integer('idrol');
            $table->timestamp('fecregistro');
            $table->integer('usuregistro')->nullable();
            $table->integer('idestado');

            $table->foreign('idestado')->references('id')->on('estado');
            $table->foreign('idtipodoc')->references('id')->on('tipodoc');
            $table->foreign('idrol')->references('id')->on('rol');

            $table->engine = 'InnoDB';
        });

        DB::table('usuario')->insert(
            array(
                'id' => '1',
                'usuario' => 'admin',
                'password' => '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', //admin
                'numdoc' => '12345678',
                'idtipodoc' => '1',
                'apellido1' => 'Shica',
                'apellido2' => 'Sivipaucar',
                'nombres' => 'Steve',
                'idrol' => '1',
                'idestado' => '1',
                'fecregistro' => '2019-09-10 00:00:00'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
