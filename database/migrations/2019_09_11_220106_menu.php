<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Menu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('nombre', 50);
            $table->string('enlaces', 2000);
            $table->integer('idtipomenu');
            $table->integer('idimagen')->nullable();
            $table->string('social', 500)->nullable();
            $table->integer('idestado');
            $table->timestamp('fecregistro');
            $table->integer('usuregistro');
            

            $table->foreign('idestado')->references('id')->on('estado');
            $table->foreign('idtipomenu')->references('id')->on('tipomenu');
            $table->foreign('idimagen')->references('id')->on('imagen');
            $table->foreign('usuregistro')->references('id')->on('usuario');

            $table->engine = 'InnoDB';
        });

        DB::table('menu')->insert(
            array(
                [
                    'id' => '1',
                    'nombre' => 'Principal',
                    'enlaces' => '[{"id":1,"texto":"SOMOS","pagina":"2","ind_sinenlace":false},{"id":2,"texto":"LOCALES","pagina":"3","ind_sinenlace":false},{"id":3,"texto":"CINE","pagina":"4","ind_sinenlace":false},{"id":4,"texto":"PROMOCIONES","pagina":"5","ind_sinenlace":false},{"id":5,"texto":"ACTIVIDADES","pagina":"6","ind_sinenlace":false},{"id":6,"texto":"BLOG","pagina":"7","ind_sinenlace":false},{"id":7,"texto":"CONTÁCTENOS","pagina":"8","ind_sinenlace":false}]',
                    'idtipomenu' => '1',
                    'idimagen' => '1',
                    'social' => '[{"nombre":"facebook","urlImg":"20092019_103355.png", "urlSocial":"#"},{"nombre":"instagram","urlImg":"20092019_103412.png", "urlSocial":"#"}]',
                    'idestado' => '1',
                    'fecregistro' => '2019-09-24 15:26:22',
                    'usuregistro' => '1'
                ]
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
