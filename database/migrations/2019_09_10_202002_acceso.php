<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Acceso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acceso', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('url');
            $table->string('icono');
            $table->string('nombre');
            $table->string('siglas');
            $table->integer('idpadre')->nullable();
            $table->string('orden');
            $table->integer('idestado');

            $table->foreign('idestado')->references('id')->on('estado');

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
