<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tipogrilla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipogrilla', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('nombre', 50);

            $table->engine = 'InnoDB';
        });

        DB::table('tipogrilla')->insert(
            array(
                [
                    'id' => '1',
                    'nombre' => 'Portada'
                ],[
                    'id' => '2',
                    'nombre' => 'Locales'
                ],[
                    'id' => '3',
                    'nombre' => 'Cine'
                ],[
                    'id' => '4',
                    'nombre' => 'Promociones'
                ],
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
