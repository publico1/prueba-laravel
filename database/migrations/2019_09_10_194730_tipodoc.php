<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tipodoc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipodoc', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('nombre');
            $table->string('siglas');

            $table->engine = 'InnoDB';
        });

        DB::table('tipodoc')->insert(
            array(
                'id' => '1',
                'nombre' => 'Documento Nacional de Identidad',
                'siglas' => 'DNI'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
