<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tiposlideer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tiposlider', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('nombre', 50);

            $table->engine = 'InnoDB';
        });

        DB::table('tiposlider')->insert(
            array(
                [
                    'id' => '1',
                    'nombre' => 'Full Screen'
                ],[
                    'id' => '2',
                    'nombre' => 'Foto con logo y Texto'
                ],[
                    'id' => '3',
                    'nombre' => 'Foto y Logo'
                ],[
                    'id' => '4',
                    'nombre' => 'Foto y Texto'
                ]
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
