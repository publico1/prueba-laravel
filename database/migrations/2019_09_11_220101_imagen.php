<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Imagen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imagen', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('nombre');
            $table->string('descripcion');
            $table->string('ruta');
            $table->integer('idestado');
            $table->integer('idtipoimagen');

            $table->foreign('idestado')->references('id')->on('estado');
            $table->foreign('idtipoimagen')->references('id')->on('tipoimagen');

            $table->engine = 'InnoDB';
        });

        DB::table('imagen')->insert(
            array(
                [
                    'id' => '1',
                    'nombre' => 'Logo Patio Panorama',
                    'descripcion' => 'Logo Patio Panorama',
                    'ruta' => '17092019_142940.png',
                    'idtipoimagen' => '1',
                    'idestado' => '1'
                ],[
                    'id' => '2',
                    'nombre' => 'Banner1',
                    'descripcion' => 'Banner1',
                    'ruta' => '17092019_143318.jpg',
                    'idtipoimagen' => '2',
                    'idestado' => '1'
                ],[
                    'id' => '3',
                    'nombre' => 'Banner2',
                    'descripcion' => 'Banner2',
                    'ruta' => '17092019_143329.jpg',
                    'idtipoimagen' => '2',
                    'idestado' => '1'
                ],[
                    'id' => '4',
                    'nombre' => 'Services1',
                    'descripcion' => 'Services1',
                    'ruta' => '17092019_154527.png',
                    'idtipoimagen' => '6',
                    'idestado' => '1'
                ],[
                    'id' => '5',
                    'nombre' => 'Services2',
                    'descripcion' => 'Services2',
                    'ruta' => '17092019_154838.png',
                    'idtipoimagen' => '6',
                    'idestado' => '1'
                ],[
                    'id' => '6',
                    'nombre' => 'Services3',
                    'descripcion' => 'Services3',
                    'ruta' => '17092019_154856.png',
                    'idtipoimagen' => '6',
                    'idestado' => '1'
                ],[
                    'id' => '7',
                    'nombre' => 'Services4',
                    'descripcion' => 'Services4',
                    'ruta' => '17092019_154915.png',
                    'idtipoimagen' => '6',
                    'idestado' => '1'
                ],[
                    'id' => '8',
                    'nombre' => 'Services5',
                    'descripcion' => 'Services5',
                    'ruta' => '17092019_154932.png',
                    'idtipoimagen' => '6',
                    'idestado' => '1'
                ],[
                    'id' => '9',
                    'nombre' => 'Services6',
                    'descripcion' => 'Services6',
                    'ruta' => '17092019_155000.png',
                    'idtipoimagen' => '6',
                    'idestado' => '1'
                ],[
                    'id' => '10',
                    'nombre' => 'Services7',
                    'descripcion' => 'Services7',
                    'ruta' => '17092019_155017.png',
                    'idtipoimagen' => '6',
                    'idestado' => '1'
                ],[
                    'id' => '11',
                    'nombre' => 'Services8',
                    'descripcion' => 'Services8',
                    'ruta' => '17092019_155044.png',
                    'idtipoimagen' => '6',
                    'idestado' => '1'
                ],[
                    'id' => '12',
                    'nombre' => 'Facebook',
                    'descripcion' => 'Facebook',
                    'ruta' => '20092019_103355.png',
                    'idtipoimagen' => '7',
                    'idestado' => '1'
                ],[
                    'id' => '13',
                    'nombre' => 'Instagram',
                    'descripcion' => 'Instagram',
                    'ruta' => '20092019_103412.png',
                    'idtipoimagen' => '7',
                    'idestado' => '1'
                ],[
                    'id' => '14',
                    'nombre' => 'Amaz 001',
                    'descripcion' => 'Amaz 001',
                    'ruta' => '20092019_163955.png',
                    'idtipoimagen' => '6',
                    'idestado' => '1'
                ],[
                    'id' => '15',
                    'nombre' => 'Amaz Logotipo',
                    'descripcion' => 'Amaz Logotipo',
                    'ruta' => '20092019_171019.png',
                    'idtipoimagen' => '1',
                    'idestado' => '1'
                ],[
                    'id' => '16',
                    'nombre' => 'Mapa001',
                    'descripcion' => 'Mapa001',
                    'ruta' => '24092019_164821.jpg',
                    'idtipoimagen' => '1',
                    'idestado' => '1'
                ],[
                    'id' => '17',
                    'nombre' => 'Icono Waze',
                    'descripcion' => 'Icono Waze',
                    'ruta' => '24092019_165919.png',
                    'idtipoimagen' => '7',
                    'idestado' => '1'
                ],[
                    'id' => '18',
                    'nombre' => 'Joven con lentes',
                    'descripcion' => 'Joven con lentes',
                    'ruta' => '25092019_115804.png',
                    'idtipoimagen' => '1',
                    'idestado' => '1'
                ],[
                    'id' => '19',
                    'nombre' => 'Familia Cine',
                    'descripcion' => 'Familia Cine',
                    'ruta' => '25092019_174945.jpg',
                    'idtipoimagen' => '1',
                    'idestado' => '1'
                ],[
                    'id' => '20',
                    'nombre' => 'Logo Footer',
                    'descripcion' => 'Logo Footer',
                    'ruta' => '26092019_121449.png',
                    'idtipoimagen' => '1',
                    'idestado' => '1'
                ],[
                    'id' => '21',
                    'nombre' => 'Foto Portada',
                    'descripcion' => 'Foto Portada',
                    'ruta' => '27092019_173259.jpg',
                    'idtipoimagen' => '1',
                    'idestado' => '1'
                ],[
                    'id' => '22',
                    'nombre' => 'Chicos Bebiendo Alegres',
                    'descripcion' => 'Chicos Bebiendo Alegres',
                    'ruta' => '30092019_144046.jpg',
                    'idtipoimagen' => '1',
                    'idestado' => '1'
                    //////////////////////////////////////////////////////
                ],[
                    'id' => '23',
                    'nombre' => 'La fresca 001',
                    'descripcion' => 'La fresca 001',
                    'ruta' => '30092019_160451.jpg',
                    'idtipoimagen' => '6',
                    'idestado' => '1'
                ],[
                    'id' => '24',
                    'nombre' => 'FD - Salon 001',
                    'descripcion' => 'FD - Salon 001',
                    'ruta' => '30092019_160543.jpg',
                    'idtipoimagen' => '6',
                    'idestado' => '1'
                ],[
                    'id' => '25',
                    'nombre' => 'wong 001',
                    'descripcion' => 'wong 001',
                    'ruta' => '30092019_160619.jpg',
                    'idtipoimagen' => '6',
                    'idestado' => '1'
                ],[
                    'id' => '26',
                    'nombre' => 'C 001',
                    'descripcion' => 'C 001',
                    'ruta' => '30092019_160836.jpg',
                    'idtipoimagen' => '6',
                    'idestado' => '1'
                ],[
                    'id' => '27',
                    'nombre' => 'Sport Life 001',
                    'descripcion' => 'Sport Life 001',
                    'ruta' => '30092019_160914.jpg',
                    'idtipoimagen' => '6',
                    'idestado' => '1'
                ],[
                    'id' => '28',
                    'nombre' => 'Loche 001',
                    'descripcion' => 'Loche 001',
                    'ruta' => '30092019_161001.jpg',
                    'idtipoimagen' => '6',
                    'idestado' => '1'
                ],[
                    'id' => '29',
                    'nombre' => 'Ibero 001',
                    'descripcion' => 'Ibero 001',
                    'ruta' => '30092019_161123.jpg',
                    'idtipoimagen' => '6',
                    'idestado' => '1'
                ],[
                    'id' => '30',
                    'nombre' => 'Bonbeef 001',
                    'descripcion' => 'Bonbeef 001',
                    'ruta' => '30092019_161150.jpg',
                    'idtipoimagen' => '6',
                    'idestado' => '1'
                ],[
                    'id' => '31',
                    'nombre' => 'Platino UVK 001',
                    'descripcion' => 'Platino UVK 001',
                    'ruta' => '30092019_161311.jpg',
                    'idtipoimagen' => '6',
                    'idestado' => '1'
                ],[
                    'id' => '32',
                    'nombre' => 'Disfruta 001',
                    'descripcion' => 'Disfruta 001',
                    'ruta' => '30092019_161355.jpg',
                    'idtipoimagen' => '6',
                    'idestado' => '1'
                ],[
                    'id' => '33',
                    'nombre' => 'Amaz 002',
                    'descripcion' => 'Amaz 002',
                    'ruta' => '30092019_161506.jpg',
                    'idtipoimagen' => '6',
                    'idestado' => '1'
                ],[
                    'id' => '34',
                    'nombre' => 'Coolbox 001',
                    'descripcion' => 'Coolbox 001',
                    'ruta' => '30092019_161528.jpg',
                    'idtipoimagen' => '6',
                    'idestado' => '1'
                ],[
                    'id' => '35',
                    'nombre' => 'La fresca logo 001',
                    'descripcion' => 'La fresca logo 001',
                    'ruta' => '30092019_163135.png',
                    'idtipoimagen' => '1',
                    'idestado' => '1'
                ],[
                    'id' => '36',
                    'nombre' => 'Pareja en el cine',
                    'descripcion' => 'Pareja en el cine',
                    'ruta' => '04102019_173905.jpg',
                    'idtipoimagen' => '1',
                    'idestado' => '1'
                ],[
                    'id' => '37',
                    'nombre' => 'Asientos',
                    'descripcion' => 'Asientos',
                    'ruta' => '07102019_140935.png',
                    'idtipoimagen' => '1',
                    'idestado' => '1'
                ],[
                    'id' => '38',
                    'nombre' => 'Dolbi',
                    'descripcion' => 'Dolbi',
                    'ruta' => '07102019_141113.png',
                    'idtipoimagen' => '1',
                    'idestado' => '1'
                ],[
                    'id' => '39',
                    'nombre' => 'Pantalla Curva',
                    'descripcion' => 'Pantalla Curva',
                    'ruta' => '07102019_141219.png',
                    'idtipoimagen' => '1',
                    'idestado' => '1'
                ]

            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
