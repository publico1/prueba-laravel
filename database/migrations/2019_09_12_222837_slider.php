<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Slider extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slider', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('nombre', 50);
            $table->integer('idestado');
            $table->integer('idtiposlider');
            $table->timestamp('fecregistro');
            $table->integer('usuregistro');

            $table->foreign('idestado')->references('id')->on('estado');
            $table->foreign('idtiposlider')->references('id')->on('tiposlider');
            $table->foreign('usuregistro')->references('id')->on('usuario');

            $table->engine = 'InnoDB';
        });

        DB::table('slider')->insert(
            array(
                [
                    'id' => '1',
                    'nombre' => 'Portada',
                    'idtiposlider' => '1',
                    'idestado' => '1',
                    'fecregistro' => '2019-09-24 15:26:22',
                    'usuregistro' => '1'
                ],[
                    'id' => '2',
                    'nombre' => 'Slider Tiendas',
                    'idtiposlider' => '2',
                    'idestado' => '1',
                    'fecregistro' => '2019-09-24 15:26:22',
                    'usuregistro' => '1'
                ],[
                    'id' => '3',
                    'nombre' => 'Slider Servicios',
                    'idtiposlider' => '4',
                    'idestado' => '1',
                    'fecregistro' => '2019-09-24 15:26:22',
                    'usuregistro' => '1'
                ],[
                    'id' => '4',
                    'nombre' => 'Portada Locales',
                    'idtiposlider' => '1',
                    'idestado' => '1',
                    'fecregistro' => '2019-09-30 15:06:54',
                    'usuregistro' => '1'
                ]
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
