<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Seccion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seccion', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->integer('idtiposeccion');
            $table->mediumText('html')->nullable();
            $table->string('nombre');
            $table->integer('idestado');
            $table->timestamp('fecregistro');
            $table->integer('usuregistro');

            $table->foreign('idestado')->references('id')->on('estado');
            $table->foreign('idtiposeccion')->references('id')->on('tiposeccion');
            $table->foreign('usuregistro')->references('id')->on('usuario');

            $table->engine = 'InnoDB';
        });

        DB::table('seccion')->insert(
            array(
                'id' => '1',
                'idtiposeccion' => '1',
                'html' => '<h4 style="text-align: center; line-height: 1.2;">Multicines UVK garantiza el buen servicio y los mejores beneficios<br>para nuestros clientes. Para conocer su cartelera y promociones ingresa aquí</h4><p style="text-align: center; line-height: 1;"><br></p><h5 style="text-align: center; line-height: 1;"><br></h5><h4 style="text-align: center; line-height: 1;"><span style="font: 25px gothamrnd-bold !important;" =""=""><a href="http://www.uvkmulticines.com/" target="_blank">http://www.uvkmulticines.com/</a></h4>',
                'nombre' => 'header',
                'idestado' => '1',
                'fecregistro' => '2019-09-25 14:21:24',
                'usuregistro' => '1'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
