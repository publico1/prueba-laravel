<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Formulario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formulario', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('nombre', 50);
            $table->integer('idestado');
            $table->integer('idtipoformulario');
            $table->integer('idimagen');
            $table->string('api');
            $table->string('html', 1000)->nullable();
            $table->string('campos', 3000)->nullable();
            $table->string('correonotif')->nullable();
            $table->timestamp('fecregistro');
            $table->integer('usuregistro');
                      

            $table->foreign('idestado')->references('id')->on('estado');
            $table->foreign('idimagen')->references('id')->on('imagen');
            $table->foreign('idtipoformulario')->references('id')->on('tipoformulario');
            $table->foreign('usuregistro')->references('id')->on('usuario');

            $table->engine = 'InnoDB';
        });

        DB::table('formulario')->insert(
            array(
                [
                    'id' => '1',
                    'nombre' => 'Registrate',
                    'idestado' => '1',                    
                    'idtipoformulario' => '1',
                    'idimagen' => '18',
                    'api' => 'patiopanorama/registate',
                    'html' => '<p>Suscríbete en la base de datos de Patio Panorama y disfruta de estar enterado de todo lo que pasa,</p>',
                    'campos' => '[{"orden":0,"tipo":"text","id_name":"frm_elem_0","placeholder":"","label":"Nombre","required":true},{"orden":1,"tipo":"email","id_name":"frm_elem_1","placeholder":"","label":"Correo Electrónico","required":true},{"orden":2,"tipo":"date","id_name":"frm_elem_2","placeholder":"","label":"Fecha de Cumpleaños","required":true}]',
                    'correonotif' => 'sshica@la-agencia.pe',
                    'fecregistro' => '2019-09-25 14:21:24',
                    'usuregistro' => '1'
                ]
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
