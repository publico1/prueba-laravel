<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RolAcceso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rol_acceso', function (Blueprint $table) {
            $table->integer('idrol');
            $table->integer('idacceso');

            $table->foreign('idrol')->references('id')->on('rol');
            $table->foreign('idacceso')->references('id')->on('acceso');

            $table->primary(array('idrol', 'idacceso'));

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
